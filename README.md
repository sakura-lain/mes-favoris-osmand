# Mes favoris Osmand +

Carte de favoris Osmand + reflétant certains de mes centres d'intérêt et voyages.

## Qu'est-ce qu'Osmand + ?

Osmand + est une application de cartographie sur mobile open source basée sur Openstreetmaps.

## Que contient ce fichier ?

- Des visites thématiques du cimetière du Père-Lachaise (en construction) ;
- Mon guide personnalisé des lieux à voir ou à éviter à Porto.

## Comment l'utiliser ?

Importer le fichier de favoris dans Osmand +.

## Liste des favoris (13 avril 2023)

### Berlin

Air B'nB - geo:52.539825,13.423332

Aire de pique-nique - geo:52.4887,13.429611

Ancien bunker, colline artificielle - geo:52.526604,13.43234

Angle de la Karl-Marx Allée et de la Straße der Pariser Kommune - geo:52.517086,13.4407625

Bagelmann - geo:52.50546,13.46937

Bateau ivre - geo:52.50053,13.42259

Berliner Dom - geo:52.519073,13.401185

Berliner Unterwelten - geo:52.54781,13.3892565

Boucherie Haroun - geo:52.55401,13.383236

Centre commercial - geo:52.549694,13.389494

Centre commercial (1) - geo:52.549618,13.41546

Citadelle de Spandau - geo:52.54098,13.212143

Coretex - geo:52.499645,13.425636

Crêperie bretonne - geo:52.497734,13.422269

E-Lok, maison de la jeunesse - geo:52.50108,13.465765

Elsenbrücke - point de vue - geo:52.495712,13.462798

Ex-aéroport de Tempelhof - bâtiment - geo:52.46663,13.4017515

Frühes nationalsozialistisches Konzentrationslager - geo:52.534134,13.419124

Gethsemanekirche - geo:52.54776,13.416205

Glaces à l'italienne - geo:52.53899,13.421341

Gusti Leder - geo:52.505352,13.469246

Île aux musées - geo:52.5199,13.398737

Italian Food - geo:52.54025,13.411206

Kebab Hasir - geo:52.501087,13.419376

KZ Sachsenhausen - geo:52.76642,13.263713

KZ Sachsenhausen - Centre d'information des visteurs - geo:52.7635,13.261893

La Commune - geo:52.49782,13.423339

La Käserie - fromagerie française - geo:52.53969,13.411785

La Poste - geo:52.53532,13.42238

Librairie Oh 21 - geo:52.500725,13.421731

Mémorial pour les Juifs d'Europe assassinés - geo:52.513973,13.378754

Monument aux déportés rroms et sintid - geo:52.517273,13.376104

Musée de la moto de la RDA - geo:52.522953,13.407729

Musée de la RDA - geo:52.51949,13.402362

Musée de la Résistance - geo:52.507607,13.362986

Musée de la Stasi - geo:52.513786,13.4866905

Musée du mur de Berlin - geo:52.533752,13.387515

Musée du quartier Kreuzberg/Friedrichshain - geo:52.50059,13.418244

Musée juif - geo:52.502357,13.395189

Mythos Germania - Vision und Verbrechen - geo:52.549484,13.387461

Office de tourisme - geo:52.516068,13.377912

Par ici : restaurants asiatiques - geo:52.50472,13.468647

Parc arboré - geo:52.484188,13.415378

Parc de Tempelhof - geo:52.478943,13.403657

Point de vue - ancienne batterie antiaérienne - geo:52.54737,13.3847885

Porte de Brandebourg - geo:52.516235,13.377735

Presse - geo:52.549232,13.388842

Presse (1) - geo:52.53561,13.422531

Reichstag - geo:52.518623,13.376051

Restaurant Breslau - geo:52.53748,13.422754

SA-Gefängnis Papestraße - geo:52.47556,13.369997

Sahara Imbiss - geo:52.488453,13.4286375

Samariter Kirche - geo:52.517963,13.466631

Sankt-Pauli Fanshop - geo:52.50134,13.419517

Schilling - geo:52.488224,13.428753

Schuttberg - geo:52.484566,13.412299

SO 36 - geo:52.500362,13.422185

Teufelsberg - ancien radar - geo:52.497204,13.239644

Topographie des Terrors - geo:52.506634,13.383959

Treptower Park - geo:52.48876,13.471013

Vestige du mur de Berlin - geo:52.534157,13.385736

Wirtshaus Heuberger - geo:52.484833,13.360041

Zionskirche - geo:52.53472,13.403766

Zur Haxe - restaurant bavarois - geo:52.544807,13.436914

### Faro et Algarve

8 Tapas - geo:37.015785,-7.9330544

A Chaminé - geo:37.025665,-7.8408394

À Do Pinto - geo:37.01616,-7.9326243

A madrugada - geo:37.02065,-7.9290466

A Murta - geo:37.02051,-7.9384756

A tasca do João - geo:37.015427,-7.9295588

Adega do mercado - Snack - geo:37.020386,-7.928829

Air B'n'B, chez Nuno - geo:37.019127,-7.9305654

Air B'nB - chez Fatima - geo:37.01938,-7.934318

Albufeira - geo:37.090027,-8.245543

Almancil - geo:37.08714,-8.031392

Almas - Terrasse sympa - geo:37.12724,-7.650848

Ancien lavoir - geo:37.12675,-7.650864

Ancora - geo:37.016853,-7.934703

Antiquaire - geo:37.01894,-7.934789

Arco da Vila - cigognes - geo:37.014664,-7.9348664

Atelier comida Santo Antônio - geo:37.021126,-7.9392176

Autocarro para Faro - geo:37.031372,-7.837876

Autocarro para Faro (1) - geo:37.03118,-7.838433

Bar - geo:37.01797,-7.9354467

Bars - geo:37.017067,-7.9353976

Boîtes de nuit - geo:37.017555,-7.935689

Botequim na Baixa - geo:37.017467,-7.9375

Boulangerie française - geo:37.124012,-7.651147

Bus 43 vers l'arrière-pays - geo:37.176155,-7.533983

Bus 43 vers Vila Real - geo:37.17613,-7.534045

Bus 65 - Arrêt aller et retour - geo:37.093124,-7.895817

Bus 113 - aller - geo:37.185753,-8.440969

Bus 113 - retour - geo:37.185764,-8.440713

Bus 113 vers Silves - geo:37.178547,-8.438386

Cacela velha - geo:37.15715,-7.5460052

Café - geo:37.019825,-7.92886

Café (1) - geo:37.09424,-7.8955774

Café (2) - geo:37.176186,-7.533982

Café cubano - geo:37.09364,-7.8954067

Café de la gare - geo:37.030186,-7.8399487

Café O Coreto - geo:37.015617,-7.935562

Café-restaurant - geo:37.020767,-7.935073

Caldas de Monchique - geo:37.286488,-8.553231

Casa da Bli - artisanat - geo:37.01681,-7.9324713

Castelo de Tavira - geo:37.125217,-7.6512136

Centre d'éducation environnementale de Marim - geo:37.032383,-7.8175316

Centre interprétatif du patrimoine islamique - Office de tourisme - geo:37.188614,-8.440109

Champs de lave de Sagres - geo:36.996033,-8.949234

Chapelle des os - geo:37.020447,-7.934897

Chefe Branco - geo:37.020844,-7.9382176

Chocolaterie artisanale - geo:37.122364,-7.6531367

Churrasqueira A Velha Casa - geo:37.015995,-7.930396

Churrasqueira Mister Frango - geo:37.019444,-7.932041

Churrasqueira O Recife - geo:37.018368,-7.9325485

Cigognes - geo:37.01506,-7.934651

Cimetière - geo:37.092636,-7.8966575

Cimetière (1) - geo:37.157078,-7.547039

Cimetière et musée juif - geo:37.02363,-7.927678

Cimetière juif - geo:52.534943,13.4141865

Comida para fora - geo:37.021183,-7.938817

Crêperie pas chère - geo:37.016247,-7.9357405

Début de la randonnée des 7 vallées suspendues - geo:37.091057,-8.412973

Dépose taxis - geo:37.156994,-7.54614

Dois Irmãos - geo:37.01709,-7.9335117

Dream Bar - geo:37.017982,-7.936983

Église - geo:37.156994,-7.545929

El Rocio - geo:37.13347,-6.483104

Entrée du château - geo:37.190468,-8.438199

Entreposage de sel - geo:37.030506,-7.9983525

Escama - geo:37.01676,-7.9353566

Estrada das 4 águas, Tavira - geo:37.117554,-7.6308603

Estrada romana pavimentada, Estoi - geo:37.09514,-7.90401

Faro - Farol - geo:36.978004,-7.8663898

Faro - Ilha Deserta - geo:36.965916,-7.8709908

Fin de la randonnée des 7 vallées suspendues - geo:37.091423,-8.453945

Flamands roses - geo:37.060463,-7.743894

flamands roses - geo:37.11507,-7.637006

Flamands roses (1) - geo:37.016445,-7.872916

Flor do Roxo - geo:37.019512,-7.931723

Forêt de pins - geo:37.088573,-8.44042

Fort de Cacela - geo:37.15711,-7.545468

Forteresse de Sagres - geo:36.99903,-8.94869

Fruits et légumes - geo:37.023727,-7.840589

Fuseta A -Centre-ville - geo:37.053593,-7.7496486

Fuseta-Moncarapacho - geo:37.0602,-7.751679

Gare de Fato - geo:37.019207,-7.940338

Gare de Silves - geo:37.175583,-8.440496

Gare routière - Proximo - geo:37.01719,-7.9377084

Golf - geo:37.029675,-8.011705

Grotte de Benagil - geo:37.086918,-8.423159

Igreja de Almancil - geo:37.0821,-8.008919

Igreja de Nossa Senhora das Ondas - geo:37.12516,-7.648548

Igreja do Carmo - geo:37.020176,-7.9347625

Igreja São Pedro - geo:37.018696,-7.934587

Ilha de Tavira - geo:37.125313,-7.6463776

Ilha Deserta - geo:36.965244,-7.8795495

Irish pub - terrasse sympa - geo:37.127396,-7.6509757

Jardin du palais - geo:37.095627,-7.8961987

Joli passage - geo:37.127354,-7.649701

Joli phare - geo:37.023182,-8.996117

Jolie place - geo:37.128147,-7.6496115

La bicyclette - geo:37.025078,-7.841394

La Poste - geo:37.019737,-7.9351215

Lagos - geo:37.09195,-8.668452

Lendas e Mitos - geo:37.18721,-8.440017

Linha 16 Aeroporto > Terminal rodoviário (Gare routière) - geo:37.0214,-7.968843

Location de voitures - geo:37.020756,-7.965231

Lodo - geo:37.017193,-7.9360366

Ludo - aller - geo:37.01495,-7.9880805

Ludo - réserve ornithologique et marais salants - geo:37.02366,-7.9956036

Ludo - retour - geo:37.01478,-7.9878736

luz - geo:37.088764,-7.699983

Magasin chinois pas cher (vêtements, papeterie, bricolage, cuisine...) - geo:37.02286,-7.9274983

Magasin d'outillage - geo:37.01863,-7.9349766

Magasin de produits en liège - geo:37.025,-7.8413186

Magasin de souvenirs - geo:37.025127,-7.841108

Magasin de souvenirs (1) - geo:37.016396,-7.934214

Mairie - geo:37.01375,-7.9345655

Marais salants - geo:37.060184,-7.742674

Marché - 2e dimanche du mois - geo:37.019547,-7.934576

Marché aux poissons - geo:37.023712,-7.841445

Marché extérieur - fruits et légumes - geo:37.02347,-7.8409677

Marché intéressant - geo:37.083416,-7.787635

Marché municipal - geo:37.020164,-7.928602

Marché municipal (1) - geo:37.186802,-8.4396105

Mine de sel - geo:37.134933,-8.008594

Mini amphithéâtre la - geo:37.126316,-7.649891

Minipreço - geo:37.01717,-7.9331884

Monchique - geo:37.318073,-8.555957

Mondial Relay - Easy Bookings - geo:37.015022,-7.933102

Mondial Relay - Ótica Lúcia - geo:37.016537,-7.931449

Musée archéologique d'Albufeira - geo:37.087082,-8.25202

Musée archéologique de Silves - geo:37.188957,-8.438942

Musée des sciences - geo:37.013157,-7.936126

Musée islamique de Tavira - geo:37.125816,-7.650221

Musée municipal - geo:37.012894,-7.933901

musée municipal - geo:37.026005,-7.8410044

Museu de Portimão - geo:37.130604,-8.53457

Museu Regional do Algarve - geo:37.016094,-7.931104

Namastey - geo:37.016964,-7.9345345

Nids de cigognes - geo:37.019794,-7.9366717

O Alminhas - geo:37.019226,-7.933468

O Seu Café - geo:37.019226,-7.9342036

Observação de aves - geo:37.024597,-8.015495

Observação de aves (1) - geo:37.024593,-8.011728

Office de tourisme - geo:37.02521,-7.8419013

Office de tourisme (1) - geo:37.18563,-8.440624

Office de tourisme de l'Algarve - geo:37.014706,-7.934788

Office du tourisme - geo:37.125843,-7.650216

Old Tavern - geo:37.01287,-7.934615

Olhão - geo:37.031696,-7.8384037

Olhão - Culatra - geo:37.022434,-7.8371277

Olhão - Culatra (1) - geo:36.99653,-7.8427153

Olhão centre - geo:37.02993,-7.8432016

P'tit Bistro' - geo:37.01717,-7.9366965

Palais d'Estoi - geo:37.096672,-7.8955164

Parc - geo:37.024845,-7.947184

Parc d'Alameda - geo:37.015118,-7.926773

Pârisserie - geo:37.016098,-7.932822

Pastelaria Coelho - geo:37.01499,-7.931292

Phare d'Alfanzina - geo:37.08663,-8.442676

Pharmacie - geo:37.016895,-7.93404

Pharmacie (1) - geo:37.016556,-7.9312987

Pharmacie (2) - geo:37.019337,-7.9283295

Poderia romana - geo:37.01844,-7.9351997

Point de vue - geo:37.156902,-7.5461526

Point de vue (1) - geo:37.157406,-7.545524

Point de vue sur le château - geo:37.18785,-8.437468

Point de vue sur les murailles - geo:37.011684,-7.933433

Poissonnerie - geo:37.020744,-7.9387207

Pont pour traverser - geo:37.089886,-7.670908

Ponte romana - geo:37.095055,-7.9057937

Ponte romana (1) - geo:37.18678,-8.438257

Ponte romano - geo:37.12684,-7.649839

Praia de Faro - geo:37.002663,-7.9865894

Praia do Carvalho - geo:37.08668,-8.431611

Quai pour Faro - geo:37.030193,-7.840171

Quiosque - geo:37.017006,-7.933944

Restaurant - geo:37.123066,-7.643783

Restaurant espagnol - geo:37.016464,-7.9340734

Restaurant mexicain - geo:37.018406,-7.935004

Restaurant népalais - geo:37.019184,-7.932432

Restaurant pas cher - geo:37.015686,-7.929989

Restaurant polonais - geo:37.015717,-7.9335127

Restaurant portugais - geo:37.01592,-7.9329543

Restaurante Alameda - geo:37.015903,-7.928696

Restaurante São Domingos - geo:37.015766,-7.9288545

Ria Moto - geo:37.01901,-7.9373765

Rue typique - geo:37.189713,-8.439651

Rues commerçantes - geo:37.016125,-7.934169

Rues typiques - geo:37.01889,-7.9371395

Ruines - geo:37.12555,-7.6508203

Ruines arabes - geo:37.157314,-7.544879

Ruines maures - geo:37.190838,-8.437545

Ruines romaines - production de garum - geo:37.025444,-8.005855

Santa Luzia - geo:37.102287,-7.6621847

São Brás de Alportel - capitale du liège - geo:37.15314,-7.887439

Sapataria - geo:37.02461,-7.8413167

Sapateiro - geo:37.01846,-7.935745

Sardinha de papel - geo:37.017506,-7.936297

Sé - geo:37.01334,-7.934674

Sentier en escaliers - geo:37.15673,-7.546583

Sentier en terre - geo:37.157276,-7.545183

Silves - geo:37.1909,-8.437969

Snack - geo:37.02016,-7.9289694

Sol e Jardim - geo:37.017056,-7.933599

Station de pompage - geo:37.126686,-7.6507807

Station de Ramalhete - geo:37.006298,-7.9673758

Supermercado - geo:37.019768,-7.9341364

Taberna Zézé - geo:37.020477,-7.933911

Take-away - geo:37.020817,-7.9354415

Tasca - geo:37.026695,-7.839274

Tasca do Ricky - geo:37.018364,-7.937547

Tascaria Chafariz - geo:37.02033,-7.936321

Taska - geo:37.019062,-7.933478

Tavira - Porta nova - geo:37.13307,-7.6481924

Tavira (1) - geo:37.120777,-7.655821

Taxis - geo:37.021084,-7.967415

Taxis (1) - geo:37.17599,-7.533945

Taxis (2) - geo:37.028027,-7.839498

Taxis (3) - geo:37.1858,-8.440878

Terminal portuaire - geo:37.01217,-7.936148

Terminal rodoviário - região - geo:37.01699,-7.9373355

Terminal rodoviário, Olhão - geo:37.028835,-7.8408456

Toilettes publiques - geo:37.017044,-7.9373274

Toilettes publiques (1) - geo:37.017235,-7.9376884

Vai e Volta - geo:37.026325,-7.839232

Vergers - geo:37.09676,-7.9006405

Vila Adentro - geo:37.01297,-7.93435

Vila Nova de Cacela - geo:37.176266,-7.5381584

Xic - Fransesinhas - geo:37.019802,-7.929988

### Finistère

Alignements de Lagatjar - geo:48.273212,-4.6096005

Café du Port - geo:47.900745,-3.9804938

Ecole de course de voile - geo:47.89959,-3.9747725

Forêt de Huelgoat - geo:48.368614,-3.743903

Île de Sein - geo:48.040283,-4.8585396

Jolie balade - geo:47.89415,-3.9871027

Manoir de Saint-Pol Roux - geo:48.274696,-4.612031

Musée de la conserverie de Loctudy - geo:47.835003,-4.1680202

Phare de la Vieille - geo:48.04066,-4.756489

Phare de Tevennec - geo:48.07153,-4.795334

Pointe du Raz - geo:48.039246,-4.7317677

Pont-Aven - geo:47.85496,-3.7468195

Stangala - geo:48.029213,-4.0544267

Guimarães

Centre-ville médiéval de Guimarães - geo:41.442875,-8.293397

Château de Guimarães - geo:41.44791,-8.290342

Palais des ducs de Bragança - geo:41.446423,-8.290884

### Le Mans et Montbizot

Arrêt tramway - geo:47.995472,0.1936192

Bar tabac Le pilier rouge - geo:48.0083,0.1970663

Boucherie charcuterie Gilles Garnier - geo:48.0026,0.2051887

Boulangerie Fortin - geo:48.00258,0.205256

Cathédrale du Mans - geo:48.00921,0.1988546

Chez Manue - geo:48.1571,0.1794115

Consigne à bagages - geo:47.99647,0.1905012

Escalier de la Grande Poterne 2 - geo:48.008026,0.1947498

Gare de Montbizot - geo:48.151676,0.1908149

Jolie tour - geo:48.00789,0.1946266

Jolie tour (1) - geo:48.009747,0.196669

Jolies tours - geo:48.008595,0.1953562

Mairie du Mans - Palais des comtes du Maine - geo:48.007366,0.1972443

Muraille romaine - geo:48.009407,0.1962101

Musée Jean-Claude-Boulard / Carré Plantagenêt - geo:48.007164,0.1981831

Office de touristisme du Mans - geo:48.008373,0.1968688

Pont dit "romain" - geo:48.140743,0.1852243

Toilettes publiques - geo:48.007412,0.1956085

### Lisbonne

15E et 714 - arrêt Belém - retour - geo:38.6974,-9.198861

781 - Vers Alfama/Sta Apólonia - geo:38.705826,-9.143124

A Ginginha - geo:38.711205,-9.128565

A Muralha (Tasca típica) - geo:38.71159,-9.127293

A Taberna - restaurant de poisson - geo:38.67356,-9.2325325

A vida portuguesa - geo:38.72105,-9.134969

Aí Mouraria - bar à vins et café - geo:38.715572,-9.13556

Air B'n'B - geo:38.710762,-9.12995

Air B'n'B (1) - geo:38.711727,-9.129356

Air BnB - Chez Sergio - Calçada da Santo André 97 - geo:38.715717,-9.132546

Alfaiataria - Vêtements et sacs indiens - Rua dos Cavaleiros (Socorro) 87 , Santa Maria Maior - geo:38.716377,-9.134554

Ancienne fabrique de poudre - geo:38.618805,-9.164612

Aqueduc du Musée de l'eau - geo:38.726692,-9.166413

Arquivo municipal fotográfico - geo:38.718143,-9.135564

Artisanat - geo:38.709892,-9.130308

Artisanat de liège - geo:38.712147,-9.139466

Artisanat népalais - geo:38.72015,-9.144108

Artisanat népalais (1) - geo:38.716038,-9.135373

Ascenseur - geo:38.711544,-9.130278

Ascenseur de Santa Justa - geo:38.71211,-9.139352

Bandida da Praça - café - geo:38.715622,-9.135539

Barbour - vêtements de moto - geo:38.715797,-9.146864

Barracão da Alfama - geo:38.71116,-9.128615

Beco das Artes - bijouterie - geo:38.711487,-9.127594

Belém - Porto Brandão - Trafaria - geo:38.69449,-9.198376

Boucherie - geo:38.722168,-9.145729

Boucherie charcuterie - geo:38.713825,-9.13708

Boulangerie - geo:38.71074,-9.129289

Bubble teas - geo:38.716167,-9.13532

Bus 708 et 208 - geo:38.732494,-9.13438

Bus 714 - Av. Infante Santo - vers Bélem - geo:38.704796,-9.167736

Bus 714 - Terminus direction Bélem/Outurela - geo:38.713642,-9.138118

Bus 727 - Escola Paula Vicente - geo:38.705658,-9.202358

Bus 732 - geo:38.70574,-9.199895

Bus 735 - vers Cais do Sodré - geo:38.713444,-9.122794

Bus 735 - Vers Primavera - geo:38.71352,-9.123441

Bus 735 - vers Primavera / Sta Apólonia - geo:38.705986,-9.143119

Bus 736 - Picoas - geo:38.714245,-9.139118

Bus 744 - Picoas / Saldanha / Aeroporto - geo:38.71572,-9.141222

Bus 758 - vers Café Brasil - geo:38.70549,-9.143135

Bus 758 et tram 24E - geo:38.714844,-9.144397

Bus 759 - geo:38.713947,-9.12195

Bus 759 - Pç do Comércio / Rossio / Sta Apólonia - geo:38.715458,-9.14174

Bus 760 et 742 - geo:38.707733,-9.196441

Bus 794 - Vers Sta Apólonia - geo:38.715683,-9.141957

Cacilhas - Cais do Sodré - geo:38.68823,-9.148193

Café - geo:38.716576,-9.134707

Café (1) - geo:38.67427,-9.231253

Café Brazil - geo:38.71437,-9.144282

Café Klandestino - geo:38.7198,-9.134976

Café Parreirinha - geo:38.71592,-9.134023

Cais do Sodré - Cacilhas - geo:38.704536,-9.146254

Cana verde - geo:38.71138,-9.1275835

Canal reliant les deux réservoirs - geo:38.718094,-9.152134

Cantinho do Aziz - geo:38.714146,-9.13559

Casa da cortiça - Maison du liège - geo:38.712547,-9.137213

Casa da cortiça - Maison du liège (1) - geo:38.715443,-9.137193

Casa dos Bicos - Fondation José Saramago - geo:38.709045,-9.132697

Cascais - geo:38.7006,-9.418419

Cash and Carry - Équipements hôteliers - geo:38.734337,-9.131669

Castelo de São Jorge - geo:38.713905,-9.133539

Catmandoo - restaurant indo-népalais - geo:38.715977,-9.137583

Cemíterio do Alto de São João - geo:38.729523,-9.121193

Cemitério dos Prazeres - Cimetière de Campo Ourique - geo:38.713356,-9.171524

Centrale électrique du Tage - geo:38.695698,-9.195181

Centre commercial - geo:38.716507,-9.135334

Centre commercial Amoreiras - geo:38.72309,-9.162113

Centre commercial Vasco de Gama - Parque das Nações - geo:38.767643,-9.096581

Centre de culture anarchiste et libertaire - geo:38.68574,-9.149348

Centro Comercial Colombo - Avenida Lusíada - geo:38.754597,-9.18848

Centro Tejo - geo:38.70669,-9.133731

Chave d'Ouro - geo:38.71132,-9.127953

Chfariz - charcuterie - geo:38.71142,-9.127653

Chinês Clandestino - geo:38.716034,-9.133936

Chinois clandestin - geo:38.71744,-9.13496

Chinois clandestin (1) - geo:38.716866,-9.135135

Conserves - geo:38.707508,-9.141173

Conserves (1) - geo:38.707554,-9.140351

Continente - geo:38.714928,-9.136619

Convento dos Cardais - geo:38.714878,-9.147738

Cova Funda - geo:38.686,-9.149263

Dans cette rue certains magasins vendent de la morue séchée. - geo:38.7074,-9.140674

Decathlon Lisboa - geo:38.73216,-9.1524725

Dershan - restaurant népalais - geo:38.72541,-9.134819

Diy Alo - restaurant indo-népalais - geo:38.715874,-9.137197

Doce Mila - geo:38.716595,-9.134513

Elevador - geo:38.71211,-9.134964

Elevador (1) - geo:38.71143,-9.135894

Elevador da Glória - bas - geo:38.716145,-9.142679

Elevador da Glória, haut - geo:38.71417,-9.14395

Entrada do Museu do Oriente - geo:38.702995,-9.171048

Entrée métro Praça do Comércio - geo:38.707497,-9.135464

Épicerie asiatique - geo:38.7169,-9.135232

Épicerie Japão - geo:38.733116,-9.132748

Escadinhas da Saúde (Mouraria) - Escalator - geo:38.71539,-9.135365

Escadinhas de São Cristóvão - geo:38.71271,-9.135905

Esplanada do Cantinho ao Sol - geo:38.71256,-9.130549

Esquadina da Fé - resto "roots' - geo:38.7197,-9.14325

Fontaine typique - geo:38.711308,-9.128157

Forteresse de Cascais - geo:38.693913,-9.419244

Fragata Dom Fernando II e Glória - geo:38.68656,-9.146117

Fromagerie - conserverie - charcuterie - geo:38.70672,-9.145285

Fruits et légumes - geo:38.716164,-9.133234

Fundação Calouste Gulbenkian - geo:38.73751,-9.154299

Funiculaire - geo:38.709778,-9.14619

Funiculaire de Lavra - geo:38.718063,-9.14097

Gare de Rossio - geo:38.714382,-9.140773

Gare de Santa Apolónia - geo:38.713627,-9.123058

Grupo excursionista Vai Tu - geo:38.709446,-9.146395

Há Tapas no Mercado !!! - geo:38.735023,-9.132373

Igreja e convento da Graça - geo:38.716396,-9.1309185

Jardim Botânico, Ajuda - geo:38.70629,-9.200665

Jardim da Estrela - geo:38.714664,-9.1597

Jardin botanique - geo:38.718346,-9.148691

Jardin botanique tropical - geo:38.699886,-9.202031

La Poste - geo:38.71596,-9.141182

Légumes secs - geo:38.70756,-9.140394

Librairie d'occasion - geo:38.71337,-9.1410885

Livraria Bertrand - geo:38.71068,-9.141146

Luz do dia - resto pas cher - geo:38.718334,-9.136751

Luz do dia - resto pas xher - geo:38.7317,-9.129049

Magasin cuisine, bricolage - geo:38.716515,-9.134825

Magasin de bricolage - geo:38.714314,-9.136701

Magasin de bricolage - Rua dos Cavaleiros (Socorro) 20-26, Santa Maria Maior - geo:38.716217,-9.133576

Magasin de cuisine - Rua dos Cavaleiros (Socorro) 23, Santa Maria Maior - geo:38.71611,-9.133623

Marché artisanal le samedi - geo:38.71796,-9.145206

Maroquinerie - geo:38.716274,-9.13377

Maruto - geo:38.70936,-9.130036

Mercado 31 de Janeiro - geo:38.73116,-9.144407

Mercado da Morais Soares - geo:38.733078,-9.131525

Mercado da Ribeira - geo:38.706974,-9.145472

Mercado de Arroios - geo:38.73522,-9.13267

Mercado de Santa Clara - geo:38.71553,-9.125621

Mercerie - geo:38.715775,-9.135447

Mercerie (1) - geo:38.717037,-9.135336

Metro Cais do Sodré - geo:38.706116,-9.145153

Mi Daí Whenzou - Restaurant clandestin ? - geo:38.716797,-9.134778

Mini mercado - geo:38.716057,-9.133262

Miradouro da Misericórdia - geo:38.71509,-9.143915

Miradouro das Portas do Sol - geo:38.71195,-9.130005

Miradouro Sophia de Mello Breyner Andresen, Lisboa - geo:38.716423,-9.131517

Monastère des Hiéronymites - geo:38.698044,-9.2057085

Mondial Relay - AG Tecidos Decorativos - geo:38.732605,-9.132526

Mondial Relay - Eagles Max - geo:38.730736,-9.134318

Mondial Relay - Jiva Mahesh - geo:38.710777,-9.145909

Mondial Relay - Lavandaria da Graça - geo:38.720676,-9.129258

Mondial Relay - O cantinho da sorte - geo:38.717667,-9.136004

Mondial Relay - Perfumarte - geo:38.731686,-9.138374

Mondial Relay - Porta Azul - geo:38.707973,-9.145225

Monument des découvreurs - geo:38.693607,-9.205739

Mr. Lu - geo:38.729786,-9.135593

Musée archéologique - geo:38.711998,-9.140571

Musée archéologique - geo:38.711998,-9.140571

Musée d'art moderne - geo:38.69563,-9.209305

Musée d'art populaire - geo:38.69363,-9.208294

Musée de l'eau - centrale à vapeur - geo:38.720016,-9.119813

Musée de l'Orient - geo:38.702934,-9.170597

Musée de la Marine - geo:38.697117,-9.208029

Musée de la morue - geo:38.707333,-9.135154

Musée de la pharmacie - geo:38.71016,-9.147264

Musée des communications - geo:38.707806,-9.150345

Musée des transports urbains - geo:38.701176,-9.179997

Musée du fado - geo:38.710968,-9.127474

Musée du sport - geo:38.715557,-9.142149

Musée national d'archéologie - geo:38.697353,-9.207088

Museu do Aljube - geo:38.71018,-9.132551

Museu do Azulejo - geo:38.724743,-9.113635

Museu do Dinheiro e Núcleo museológico da muralha - Musée de la monnaie. - geo:38.708595,-9.138895

Museu do Teatro Romano - geo:38.710556,-9.132351

Museu Geológico - geo:38.713108,-9.1497555

Museu Nacional de Arte Antiga - geo:38.704556,-9.161911

Museu Nacional de Etnologia - geo:38.704807,-9.208217

Museu Nacional de História Natural e da Ciência - geo:38.71771,-9.150469

Nappes, torchons, sacs en tissu... - geo:38.71505,-9.1370735

Nelcork Portugal - objets en liège - geo:38.716343,-9.134394

New Himalia - geo:38.71929,-9.143271

Noca's Café - geo:38.70906,-9.131266

Novo Conceito - geo:38.71246,-9.125615

O Coradinho - Restaurant pas cher - geo:38.721092,-9.144558

O gambuzino - petiscos e tapas - geo:38.72169,-9.134912

Observatório Astronómico de Lisboa - geo:38.710518,-9.18744

Office de tourisme - geo:38.707684,-9.137771

Os Amigos da Severa - Bar à ginja - geo:38.71623,-9.134659

Palácio Fronteira - geo:38.740086,-9.180279

Palácio Nacional da Ajuda - geo:38.707584,-9.197661

Pangzi Noodles - geo:38.71761,-9.134936

Panthéon - geo:38.71501,-9.124664

Parc Monsanto - geo:38.72863,-9.192681

Parque Marechal Cormona - geo:38.693157,-9.422908

Passerelle d'accès au Musée de l'Orient - geo:38.703358,-9.168029

Passerelle d'accès aux tramways et bus - geo:38.70352,-9.167946

Pastéis de Belém - geo:38.697517,-9.203257

Pastelaria - boulangerie - geo:38.716316,-9.135161

Pastelaria - café - geo:38.71848,-9.144748

Pâtisserie - geo:38.710278,-9.129773

Pâtisserie Orion - geo:38.71082,-9.146323

Petite librairie - geo:38.71278,-9.136149

Pho Pu - geo:38.71759,-9.134825

Pingo Doce - geo:38.713657,-9.140276

Pingo Doce (1) - geo:38.712147,-9.135107

Pingo Doce (2) - geo:38.71429,-9.122135

Pingo Doce (3) - geo:38.73398,-9.1331

Pingo Doce (4) - geo:38.705696,-9.144716

Point de vue - geo:38.71528,-9.134773

Point de vue (1) - geo:38.719128,-9.141336

Point de vue (2) - geo:38.71591,-9.137184

Point de vue (3) - geo:38.716015,-9.137457

Point de vue (4) - geo:38.718716,-9.141383

Point de vue (5) - geo:38.70679,-9.197809

Point de vue (6) - geo:38.712944,-9.134181

Point de vue (7) - geo:38.695816,-9.193305

Point de vue sur lechâteau - geo:38.715763,-9.136683

Points de vue - geo:38.712746,-9.172999

Points de vue (1) - geo:38.71349,-9.141558

Poissonnerie - geo:38.71175,-9.128091

Poissonnerie (1) - geo:38.716324,-9.135028

Ponte 25 de Abril - geo:38.691605,-9.177513

Primavera - geo:38.732853,-9.131607

Quiosque Time Out - geo:38.70729,-9.146768

Rei do bacalhau - geo:38.707695,-9.139805

Reservatório da Mãe d'Água das Amoreiras - Musée de l'eau - geo:38.721237,-9.155679

Réservoir du Patriarcal - Musée de l'eau - geo:38.716206,-9.148918

Restaurant de l'Institut Goethe - geo:38.72117,-9.140894

Restaurant libanais - geo:38.715214,-9.135876

Restaurant népalais - geo:38.715557,-9.138139

Restaurant sénégalais - geo:38.715824,-9.138479

Restaurante O Arco - geo:38.71092,-9.129019

Restaurants bengalis - geo:38.719414,-9.134848

Resto pas cher - geo:38.719406,-9.137635

Resto pas cher (1) - geo:38.719322,-9.13746

Rua 1, Belém - geo:38.700317,-9.204074

Rua da Palma - restaurants tibétains et népalais - geo:38.719593,-9.135592

Rue des bazars et restaurants asiatiques et arabes - geo:38.717155,-9.134965

Rue intéressante - geo:38.727173,-9.13582

Rue métissée avec restaurants asiatiques et africaind - geo:38.71627,-9.13851

Samedi - Marché d'artisanat - geo:38.714806,-9.144095

Sardinha - geo:38.712322,-9.125691

Sé de Lisboa - geo:38.709866,-9.132823

Sintra - geo:38.799328,-9.385522

Site phénicien - geo:38.685955,-9.152391

Solar 31 - Restaurant de poissons - geo:38.715397,-9.13798

Sortie métro Arroios - côté Praça do Chile/Rua Morais Soares - geo:38.73343,-9.133996

Stands de ginginha - geo:38.711178,-9.128147

Suparmarket - geo:38.716114,-9.133729

Sushizen - geo:38.71097,-9.128928

Tapisco - macaron Michelin - geo:38.715687,-9.1465845

Tasca do Beco - restaurant pas cher - geo:38.716927,-9.138677

Tascardoso - restaurant pas cher - geo:38.715973,-9.147685

Terminus du tramway 28E - geo:38.714245,-9.169504

Terminus du tramway 28E (1) - geo:38.71522,-9.13612

The Decadente - geo:38.714928,-9.144619

Théâtre romain - geo:38.71043,-9.132287

Ti Camila - geo:38.71136,-9.127871

Timbres et boîte aux lettres - geo:38.713898,-9.122513

Time Out Market - geo:38.70699,-9.145947

Toilettes - geo:38.714096,-9.122854

Toilettes (1) - geo:38.674294,-9.231052

Toilettes (2) - geo:38.69501,-9.198321

Torre de Belém - geo:38.69156,-9.215919

Trafaria - Porto Brandão - Belém - geo:38.674774,-9.230868

Tram 18E et bus 529 - geo:38.706444,-9.199141

Tram - arrêt hyéronymites - geo:38.697323,-9.204923

Tram et bus - arrêt Belém (aller) - geo:38.69746,-9.19856

Tram et bus - retour de Belém - geo:38.69737,-9.203496

Trésor royal - geo:38.707554,-9.198762

Tudo - restaurant indo népalais - geo:38.71986,-9.134947

Vêtements indiy - geo:38.716354,-9.134016

Xandite da Trafaria - Café - geo:38.67221,-9.233134

Yak & Yeti - geo:38.714905,-9.131604

### Octo

Autour du Yangtse - geo:48.872044,2.3350976

Berliner - geo:48.86741,2.3328178

Bubble tea - geo:48.868664,2.3367107

Chez TLU - geo:48.866253,2.3441274

Ciamt - Médecine du Travail - geo:48.873142,2.3607516

Duck Conf - geo:48.837822,2.4401908

Épicerie asiatique - geo:48.8688,2.3336785

Épicerie coréenne - geo:48.868187,2.336303

L'Othentique Vietnam - geo:48.868164,2.335667

Le Vaisseau - geo:48.863174,2.3400738

Mezze grecs - geo:48.867996,2.3355014

Nam Nam - Coréen à emporter - geo:48.868595,2.3366907

Octo côté Gaillon - geo:48.868603,2.3341575

Octo côté Opéra - geo:48.868683,2.3334057

Plénière - geo:48.832916,2.4752855

Seunsep - resto thaï - geo:48.867973,2.3355465

STET - geo:48.89074,2.2441711

Stet - geo:48.88707,2.2436218

The Alley - Bubble Tea - geo:48.867184,2.3347778

Tour Pitard - geo:48.83506,2.3111186

Traiteurs chinois - geo:48.864307,2.3415468

WOW!Tea - Bubble Tea - geo:48.868935,2.3361661

Xing Fu Tang - Bubble Tea - geo:48.866913,2.3352015

### Paris

Babou - geo:48.86395,2.41626

Babou - geo:48.86395,2.41626

Bazar d'Électricité - geo:48.85192,2.3656278

CogCharonne - geo:48.854305,2.3884335

Coop. Latte Cisternino - geo:48.866585,2.3767996

Cours de portugais - geo:48.86113,2.369442

Dr Elriz - geo:48.84006,2.396533

Eatitaly - geo:48.858646,2.3545067

Ha Noi 1988 - geo:48.856754,2.3415668

Hackspark - geo:48.852993,2.3903823

Hanoi Authentique - geo:48.86637,2.3388493

Harry Star - geo:48.8686,2.3889804

kyobashi - geo:48.866238,2.3766806

La Mandragore - bijouterie celtique - geo:48.867706,2.3964357

Let Me Know - geo:48.869335,2.3504906

Mercerie - geo:48.874413,2.3865223

Musée de La Poste - geo:48.841396,2.3172407

Musée du Carnavalet - geo:48.85709,2.3628411

Neelkanth - geo:48.867332,2.3991096

Neuropsychologue - geo:48.788578,2.4762206

passementerie japonaise - geo:48.859146,2.3566236

Primland - épicerie portugaise - geo:48.87209,2.4396837

Saint-Quentin Radio - geo:48.877407,2.3555841

Sangeetha - geo:48.880325,2.3577979

Sud Informatique - geo:48.87513,2.3669744

Ultramod - Mercerie - geo:48.869156,2.3360274

### Père-Lachaise - parcours communard

André Gill - geo:48.86015,2.39705

Auguste Blanqui - geo:48.86119,2.39603

Auguste Okolowicz - geo:48.8644,2.39498

Augustin Moreau-Vauthier - geo:48.85979,2.39369

Charles Longuet - geo:48.86096,2.39684

Crématorium et colombarium - geo:48.862743,2.3957398

Eugène Pottier - geo:48.8605,2.39807

Felix Pyat - geo:48.86276,2.39369

Jean Allemane - geo:48.86209,2.39841

Jean Baptiste Clément - geo:48.859802,2.399945

Jean-Baptiste Dumay - geo:48.862717,2.3957417

Jules Joffrin - geo:48.86027,2.39827

Jules Vallès - geo:48.86228,2.39043

Le monument de Paul Moreau-Vauthier "Aux victimes des révolutions" - geo:48.86356,2.3911493

Léo Frankel - geo:48.85934,2.39843

Mausolée de la famille Thiers - geo:48.86111,2.3929722

Monument à la mémoire des gardes nationaux - geo:48.86323,2.39083

Mur des Fédérés - geo:48.859684,2.4000769

Paul Lafargue et Laura Marx - geo:48.859848,2.4000068

Paule Mink - geo:48.862732,2.3957925

Site des derniers combats - Tombeau de Charles Nodier - geo:48.86283,2.39325

Victor Noir - geo:48.86083,2.39653

Père-Lachaise - sites remarquables

Citerne (1688) - geo:48.86083,2.3941543

Reine Fevez (1804) - geo:48.86132,2.39005

### Porto

Air B'n'B - geo:41.142815,-8.612416

Annexe du musée de la ville consacrée au Douro - geo:41.14016,-8.615313

Archéosite - geo:41.14294,-8.610567

Arroba e Meia - geo:41.1354,-8.611393

Bus 500 - départ - geo:41.14491,-8.610899

Café Calhambeque - geo:41.142662,-8.61241

Calem - geo:41.138016,-8.610969

Cana Verde - geo:41.14505,-8.6147

Casa Chinesa - geo:41.149185,-8.607803

Casa do Infante - Musée de la ville - geo:41.14074,-8.614142

Cathédrale (Sé) - geo:41.142826,-8.611216

Centre commercial - geo:41.149723,-8.604664

Churrasqueira Brasa - geo:41.14553,-8.607616

Churrasqueira Brasil - geo:41.145016,-8.615196

Churrasqueira do Infante - geo:41.141598,-8.614391

Concept 31 - geo:41.144993,-8.612514

Confeitaria Império - geo:41.150127,-8.607676

Cris Bar - geo:41.142433,-8.614091

Cultura Portuguesa Arts & Crafts - Produits en liège - geo:41.144672,-8.611425

Fnac - geo:41.14694,-8.607053

Fontaine - geo:41.14248,-8.612137

Funiculaire - geo:41.14316,-8.608268

Gare de São Bento - geo:41.145603,-8.610328

Hôtel de Ville - geo:41.149933,-8.610714

Igrejas do Carmo e dos Carmelitas - geo:41.147385,-8.616336

Largo do Colégio - geo:41.142357,-8.612866

Leitão - geo:41.144703,-8.612475

Livraria Bertrand - geo:41.147697,-8.613629

Livraria Lello - geo:41.146797,-8.6147995

Livraria Porto Editora - geo:41.14795,-8.612773

Magestic café - geo:41.14724,-8.606643

Marché artisanal le samedi - geo:38.713703,-9.137798

Mercado de Matosinhos - geo:41.18692,-8.692995

Mercado de São Sebastião - geo:41.143806,-8.611197

Minipreço - geo:41.145145,-8.612252

Minipreço (1) - geo:41.14935,-8.60774

Muraille - geo:41.14178,-8.609289

Musée de la photographie - geo:41.1447,-8.615846

Musée du CHU de Porto - geo:41.146763,-8.618152

Musée et église de São-Francisco - geo:41.14102,-8.616348

Navette fluviale - geo:41.13902,-8.613823

Nortel - équipements pour professionnels de la restauration - geo:41.150208,-8.608027

O Forte dos Queijos - geo:41.14333,-8.60981

Office de tourisme - geo:41.142735,-8.612149

Palais de la Bourse - geo:41.141354,-8.615522

Pingo Doce - geo:41.14966,-8.607613

Pingo Doce (1) - geo:41.14674,-8.604643

Place de la Liberté - geo:41.146523,-8.611392

Point de vue - geo:41.14206,-8.612931

Point de vue (1) - geo:41.142307,-8.612028

Point de vue (2) - geo:41.14308,-8.6113615

Point de vue (3) - geo:41.143143,-8.615883

Point de vue (4) - geo:41.142433,-8.612629

Point de vue depuis le haut du pont - geo:41.139668,-8.609274

Point de vue sur Porto - geo:41.13831,-8.608614

Points de vue - geo:41.14086,-8.609731

Português de Gema - geo:41.142586,-8.613386

Quai de Gaia - geo:41.137867,-8.613197

Quais da Estiva - geo:41.140316,-8.613554

Quinta dos Corvos - geo:41.136677,-8.614881

Rue à boulangeries - geo:41.144047,-8.60755

Rue commerçante - geo:41.148815,-8.606133

Rue des magasins de musique - geo:41.147095,-8.608835

Rues typiques - geo:41.14266,-8.613039

Rues typiques (1) - geo:41.135876,-8.609985

Rues typiques (2) - geo:41.141804,-8.609886

Taberna Santo António - geo:41.143425,-8.618114

Téléphérique - geo:41.137924,-8.609661

Torre dos clérigos - geo:41.145683,-8.614531

Tramway - geo:41.140785,-8.615505

Village de pêcheurs typique - geo:41.18352,-8.694317

Voltaria - Petisqueira portuguesa - geo:41.14439,-8.61229

Vue sur le coucher du soleil - geo:41.13715,-8.609299

### Portugal

Baleal - Mini-village de pêcheurs et joli archipel - geo:39.375454,-9.341078

Braga - geo:41.54849,-8.416176

Forteresse de Peniche - ancienne prison salazariste - geo:39.35303,-9.381334

Jolie région - geo:38.2937,-7.3963275

Nazaré - vagues spectaculaires - geo:39.600582,-9.070809

Nazaré - vieille ville - geo:39.60308,-9.06968

Réserve naturelle - geo:39.415375,-9.509889

Setúbal - Jolie ville - geo:38.531128,-8.88482

### Potsdam et Babelsberg

Ancienne prison du KGB - geo:52.416576,13.063991

Ancienne prison nazie et est-allemande - geo:52.400467,13.0522785

Karl-Liebknecht-Stadion - geo:52.399117,13.094841

Pont des espions - Glienicker Brücke - geo:52.413475,13.0903

### Quimper

Ecomiam - Mondial Relay - geo:48.016655,-4.0830946

Marché du Braden (le dimanche) - geo:47.98499,-4.0836186
